# Symbols
: ${is_a_git_repo_symbol:='❤'}
: ${has_untracked_files_symbol:='∿'}
: ${has_adds_symbol:='+'}
: ${has_deletions_symbol:='-'}
: ${has_deletions_cached_symbol:='✖'}
: ${has_modifications_symbol:='M'}
: ${has_modifications_cached_symbol:='☲'}
: ${ready_to_commit_symbol:='→'}
: ${is_on_a_tag_symbol:='⌫'}
: ${needs_to_merge_symbol:='><'}
: ${has_upstream_symbol:='⇅'}
: ${detached_symbol:='@'}
: ${can_fast_forward_symbol:='»'}
: ${has_diverged_symbol:='^'}
: ${rebase_tracking_branch_symbol:='↶'}
: ${merge_tracking_branch_symbol:='>'}
: ${should_push_symbol:='↑'}
: ${has_stashes_symbol:='★'}

# Flags
: ${display_has_upstream:=false}
: ${display_tag:=false}
: ${display_tag_name:=true}
: ${two_lines:=true}
: ${finally:=''}
: ${use_color_off:=false}
autoload colors && colors
for COLOR in RED GREEN YELLOW BLUE MAGENTA CYAN BLACK WHITE; do
    eval $COLOR='%{$fg_no_bold[${(L)COLOR}]%}'  #wrap colours between %{ %} to avoid weird gaps in autocomplete
    eval BOLD_$COLOR='%{$fg_bold[${(L)COLOR}]%}'
done
eval RESET='%{$reset_color%}'
on=$WHITE
off=$WHITE
red=$RED
green=$GREEN
yellow=$YELLOW
violet=$CYAN
branch_color=$BLUE
reset=$RESET


if [ $UID -eq 0 ]; then NCOLOR="red"; else NCOLOR="green"; fi
if [ -z "$SSH_TTY" ] && [ -z "$SSH_CONNECTION" ] && [ -z "$SSH_CLIENT" ] ; then
  local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
else
  local return_code="%(?..%{$fg[green]%}%? ↵%{$reset_color%})"
fi

# primary prompt
PROMPT='$(build_prompt)
$FG[032]%~ \
$FG[105]%(!.#.$)%{$reset_color%} '
PROMPT2='%{$fg[red]%}\ %{$reset_color%}'
RPS1='${return_code}'
# »

# color vars
eval my_gray='$FG[237]'
eval my_orange='$FG[214]'

# right prompt
if type "virtualenv_prompt_info" > /dev/null
then
  if [[ $EUID -ne 0 ]] && [ -z "$SSH_TTY" ] && [ -z "$SSH_CONNECTION" ] && [ -z "$SSH_CLIENT" ]; then
    RPROMPT='$(virtualenv_prompt_info)$my_gray%n@%m%{$reset_color%}%'
  else
    RPROMPT='$(virtualenv_prompt_info)$my_red%n@%m%{$reset_color%}%'
  fi
else
  if [[ $EUID -ne 0 ]] && [ -z "$SSH_TTY" ] && [ -z "$SSH_CONNECTION" ] && [ -z "$SSH_CLIENT" ]; then
    RPROMPT='$my_gray%n@%m%{$reset_color%}%'
  else
    RPROMPT='$my_red%n@%m%{$reset_color%}%'
  fi
fi
